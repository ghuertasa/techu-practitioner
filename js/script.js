function getStorage(){
	var storage = document.getElementsByName('storage');
	var storageValue;
	for(i = 0; i < storage.length; i++) {
        if(storage[i].checked)
                storageValue = storage[i].value;
    }
	return storageValue;
}

function guardar(){
  var storage=getStorage();
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value.trim();
  var valor = txtValor.value.trim();
  if(clave!=''&&valor!=''){
	  if(storage!='sessionStorage')
		localStorage.setItem(clave, valor);
	  else
		sessionStorage.setItem(clave, valor);
   }else{
		alert("Ingrese clave y/o valor a guardar");
   }
   limpiarTexto();
}
function leer(){
	var storage=getStorage();
	var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
	var clave = txtClave.value.trim();
	var valor;
	if(clave!=''){
		if(storage!='sessionStorage')
			valor = localStorage.getItem(clave);
		else
			valor = sessionStorage.getItem(clave);
		txtValor.value=valor;
	}else{
		alert("Ingrese clave a leer");
	}
}
function eliminar(){
  var storage=getStorage();
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value.trim();
  if(clave!=''){
	  if(storage!='sessionStorage')
			localStorage.removeItem(clave);
	  else
			sessionStorage.removeItem(clave);
	  alert("Clave "+clave+" eliminada");
  }else{
	alert("Ingrese clave a eliminar");
  }
  limpiarTexto();
}

function limpiar() {
  var storage=getStorage();
  if(storage!='sessionStorage')
	localStorage.clear();
  else
	sessionStorage.clear();
  alert(storage+" eliminado");
  limpiarTexto();
}

function longitud() {
  var longitud;
  var storage=getStorage();
  if(storage!='sessionStorage')
	longitud=sessionStorage.length;
  else
	longitud=sessionStorage.length;
  alert("Longitud de "+storage+": "+longitud);
}

function limpiarTexto(){
	var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
	var txtValor = document.getElementById("txtValor");
	txtClave.value='';
	txtValor.value='';
}
